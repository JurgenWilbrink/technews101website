package TechNews101WebSite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebsiteRunThing {

    public static void main(String[] args) {
        SpringApplication.run(WebsiteRunThing.class, args);
    }

}
