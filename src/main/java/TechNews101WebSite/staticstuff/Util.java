package TechNews101WebSite.staticstuff;

import TechNews101WebSite.model.Administrator;
import TechNews101WebSite.model.Article;
import org.springframework.ui.Model;

import javax.servlet.http.HttpSession;

public class Util {

    /**
     * String in, enum Category out, simple as that. if no match is found for the string, returns general
     *
     * @param string
     * @return
     */
    public static Article.Category getEnumCategoryFromString(String string) {
        // if input = "Games, Space, SocialMedia or General"
        System.out.println("** Convert Enum to String: '" + string + "'");
        if (string.equals(Article.Category.GAMES.getStringVal())) {
            return Article.Category.GAMES;
        } else if (string.equals(Article.Category.SPACE.getStringVal())) {
            return Article.Category.SPACE;
        } else if (string.equals(Article.Category.SOCIALMEDIA.getStringVal())) {
            return Article.Category.SOCIALMEDIA;
        } else if (string.equals(Article.Category.GENERAL.getStringVal())) {
            return Article.Category.GENERAL;
        } else { // Default
            return Article.Category.GENERAL;
        }
    }

    /**
     * checks if a session exists
     *
     * @param session
     * @return
     */
    public static boolean sessionCheck(HttpSession session) {
        if (session.getAttribute("username") != null) {
            System.out.println("$$Session username Detected: '" + session.getAttribute("username") + "'");
            return true;
        } else {
            System.out.println("$$No Session Detected..");
            return false;
        }
    }

    /**
     * checks if an existing username is stored within the session
     *
     * @param session
     * @return
     */
    public static boolean existingUserCheckFromSession(HttpSession session) {
        if (DataClass.userLibrary.getUserByUserName(String.valueOf(session.getAttribute("username"))) != null) {
            System.out.println("$$Session username exists: '" + session.getAttribute("username") + "'");
            return true;
        } else {
            System.out.println("$$No username Detected..");
            return false;
        }
    }

    /**
     * this is used for the op bar. if a session exists, (if the user is logged in) the top bar has different buttons,
     * therefor this is at the top of each method / action
     *
     * @param session
     * @param model
     * @return
     */
    public static Model checkForSessionAddAtributesAccordingly(HttpSession session, Model model) {
        if (Util.sessionCheck(session) && Util.existingUserCheckFromSession(session)) {
            model.addAttribute("loggedIn", true);
            model.addAttribute("username", session.getAttribute("username"));
        } else {
            model.addAttribute("loggedIn", false);
        }
        return model;
    }

    /**
     * simply checks if a username that is stored in a session is an administrator.
     *
     * @param session
     * @return
     */
    public static boolean checkSessionForAdministrator(HttpSession session) {
        return DataClass.userLibrary.getUserByUserName(String.valueOf(session.getAttribute("username"))) instanceof Administrator;
    }
}