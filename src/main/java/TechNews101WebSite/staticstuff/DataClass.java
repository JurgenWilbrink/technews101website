package TechNews101WebSite.staticstuff;

import TechNews101WebSite.model.*;

import java.util.ArrayList;
import java.util.Arrays;

public class DataClass {

    //static libraries where website data is kept 'safe'
    public static ArticleLibraby articleLibraby = new ArticleLibraby();
    public static UserLibrary userLibrary = new UserLibrary();

    /**
     * this is pure for testing.
     * amount of copies of a specific article
     */
    private static int amountOfEurovisionArticleCopies = 10;
    private static int amountOfTestReplies = 10;

    //method's to add the pre-programmed objects to the static libraries above.

    /**
     * method to add pre-programmed Users
     *
     * @param user a pre-programmed User
     */
    private static void addaUser(User user) {
        userLibrary.addUserWithUserNameCheck(user);
    }

    /**
     * method to add pre-programmed Articles
     *
     * @param article a pre-programmed Article
     */
    private static void addAnArticle(Article article) {
        articleLibraby.addArticleToLibrary(article);
    }

    /**
     * method to add pre-programmed Replies
     *
     * @param id    targetId for the reply to be attached to
     * @param reply a pre-programmed reply
     */
    private static void addAnReplyToPost(int id, Reply reply) {
        articleLibraby.addReply(id, reply);
    }

    /**
     * the actual pre-programmed users
     */
    private static void initUsers() {
        /*0*/
        addaUser(new Administrator("Admin-1", "Jurwix", "1234", 18, 999, 1338, "83hgD83hs3jgw3", true));
        /*1*/
        addaUser(new Administrator("OverLord", "Mark", "Special", 22, 455, 1337, "3DfAu923s30j39", false));
        /*2*/
        addaUser(new User("Henk101", "Henk", "HenksPassword", 34, 3));
        /*3*/
        addaUser(new User("TimDeSchrijver", "Tim", "timytomytimy", 45, 5));
        /*4*/
        addaUser(new User("PindanewsLezer", "Samanta", "ILovePindas", 25, 7));
        /*5*/
        addaUser(new User("Space-Agent", "Duncan", "Interstaller123", 20, 4));
        // In documentatie
        /*6*/
        addaUser(new User("Sneaky09", "Zachary Scuderi", "PickPock3tJhin", 24, 234));
        /*7*/
        addaUser(new User("HuaweiHater", "Jan", "ILoveHuawei<3", 21, 1));
        /*8*/
        addaUser(new User("GhostCrawler", "Unknown", "IHATELEAGUEOFLEGENDS", 54, 2344));
    }

    /**
     * the actual pre-programmed articles
     */
    private static void initArticles() {
        addAnArticle(new Article("ISS around Earth", "This article is about The ISS",
                "The International Space Station is an orbiting laboratory and construction site that synthesizes the scientific expertise of 16 nations to maintain a permanent human outpost in space.\n" +
                        "\n" +
                        "While floating some 240 miles (390 kilometers) above Earth's surface, the space station has hosted a rotating international crew since November 2000.\n" +
                        "\n" +
                        "Astronauts and supplies are ferried by the U.S. space shuttles and the Russian Soyuz and Progress spacecraft. Astronauts who reach the facility aboard one of these missions typically live and work in orbit for about six months.\n" +
                        "\n" +
                        "Simply by spending time in orbit, astronauts reveal much more about how humans can live and work in space. Crews have learned the difficulties of diet, in a world in which their sense of taste is decreased, and of getting a good night's sleep while secured to a non-floating object.\n" +
                        "\n" +
                        "But the crew is also occupied with a full suite of scientific experiments, the ongoing improvement and construction of the station, and a rigorous regime of physical training. Astronauts must exercise for two hours each day to counteract the detrimental effects of low gravity on the body's skeleton and circulatory system.",
                userLibrary.getUserByUserName("Space-Agent"),
                "/images/issimage.jpg",
                "image of the ISS, with earth in the background",
                new ArrayList<Article.Category>(Arrays.asList(Article.Category.SPACE))));

        addAnArticle(new Article("Spring Split LCS News", "This article is about The LCS",
                "As the Spring Split comes to a close, we’re experimenting with something a little different. We know it can be difficult to keep up with all the English broadcasts, so we want to make it a little easier with Must Watch Matches: a packaged rebroadcast of the 3 most exciting games from each week of LPL, LCK, EU LCS, and NA LCS. Must Watch Matches will air twice on Tuesdays for both EU and NA audiences at 6:00 PM CET and 2:00 AM CET.",
                userLibrary.getUserByUserName("Sneaky09"),
                "/images/springsplitimage.jpg",
                "logo of the Spring split competition",
                new ArrayList<Article.Category>(Arrays.asList(Article.Category.GAMES))));

        addAnArticle(new Article("Huawei is being f*cked...", "there has a big ban on the company Huawei.",
                "Developing story. It looks like Google has effectively “cancelled” Huawei in a move many have been expecting for awhile now." +
                        "According to various sources, Google has suspended ties with Huawei particularly on “requires the transfer of hardware and software products.”" +
                        "International news agency Reuters adds:" +
                        "Huawei Technologies Co Ltd will immediately lose access to updates to the Android operating system, and the next version of its smartphones outside of China will also lose access to popular applications and services including the Google Play Store and Gmail app," +
                        "So why did this happen?" +
                        "The tl;dr is that Huawei was part of a list called the “Entity List”, which is the popular term for US Blacklisted companies and entities. This was announced last Wednesday by the US Commerce Department following President Donald Trump’s signing of the list." +
                        "Representatives from Huawei had this to say over the blacklisting:" +
                        "against the decision made by the Bureau of Industry and Security of the U.S. Department of Commerce." +
                        "This would also affect new Huawei smartphones released like the P30 series and even spinoff brands like One Plus Honor.",
                userLibrary.getUserByUserName("HuaweiHater"),
                "/images/huaweiimage.jpg",
                "logo of the Huawei Company",
                new ArrayList<Article.Category>(Arrays.asList(Article.Category.SOCIALMEDIA))));

        addAnArticle(new Article("League Patch 9.10", "The patch notes of league of legends 9.10 explained for you in great detail.",
                "The magical (and adorable) cat will arrive on the Rift this patch, and promises to shake things up a bit. The whimsical support comes with the ability to permanently attach to an allied champion, becoming almost completely untargetable, but losing the ability to move around. Riot reckons she’ll be a good fit for new players getting to grips with the game, but it’ll be interesting to see what she can do in the hands of League of Legends veterans." +
                        "Elsewhere, Yuumi helps bring in a brand-new skin line. Ezreal, Graves, Lux, Katarina, and Jayce join her in the new Battle Academia cosmetic set, which returns League of Legends to the same anime stylings that players have enjoyed with skin lines like the Star Guardians.",
                userLibrary.getUserByUserName("GhostCrawler"),
                "/images/negenpunttienimage.PNG",
                "patch notes 9.10 summery",
                new ArrayList<Article.Category>(Arrays.asList(Article.Category.GAMES))));

        //An article in a for loop to test how more articles will look.
        for (int i = 0; i < amountOfEurovisionArticleCopies; i++) {
            addAnArticle(new Article("Music installation Failed at Eurovision", "How a music installation failed at eurovision cousing 20 people to get injured. (Test Article: " + (i + 1) + ")",
                    "With Duncan Laurence's win, the Eurovision Song Contest will be moving to the Netherlands next year. But in which Host City will the 2020 Eurovision Song Contest take place? Dutch public broadcasters NPO, AVROTROS and NOS are officially kicking off the City Bid process!" +
                            "Next year’s contest will be a joint venture between 3 Dutch EBU Members: NPO, AVROTROS and NOS. They are now inviting Dutch cities and regions to apply as Host City for the 2020 Eurovision Song Contest." +
                            "City Bid process in June/July" +
                            "The selection process will consist of 3 phases:" +
                            "In the first half of June, all interested cities/regions will receive a document from NPO/AVROTROS/NOS with criteria they will need to meet for hosting. This will form the foundation for each city’s bid." +
                            "The cities/regions in the running for Host City will then have 4 weeks to compile their “bid books”, to be submitted in the first half of July." +
                            "In mid-July, the organizers have the option to visit the cities and regions that have applied. The host city for the 65th Eurovision Song Contest will be decided in consultation with the EBU following an assessment of all the bids." +
                            "Interested Dutch cities and regions can email the organization." +
                            "Previous Dutch Host Cities/" +
                            "The Netherlands has hosted the Eurovision Song Contest 4 times before: in 1958, 1970, 1976 and 1980. The first edition took place in Hilversum, the second in the Dutch capital Amsterdam and the third and fourth editions in the Hague.",
                    userLibrary.getUserByUserName("OverLord"),
                    "/images/songfestivalimage.jpg",
                    "image of duncon singing",
                    new ArrayList<Article.Category>(Arrays.asList(Article.Category.GENERAL))));
        }
    }

    /**
     * the actual pre-programmed replies
     */
    private static void initReplies() {
        addAnReplyToPost(0, new Reply("How does something orbit around the earth if it is flat?", userLibrary.getUserById(8)));

        int startingIntForAddingTestReplies = articleLibraby.getArticles().size();

        //For-loop for adding testreplies
        for (int i = startingIntForAddingTestReplies; i < startingIntForAddingTestReplies + amountOfTestReplies; i++) {
            addAnReplyToPost(i, new Reply("If you read this nested replies work", userLibrary.getUserById(6)));
        }

        addAnReplyToPost(0, new Reply("How does something orbit around the earth if it is flat?", userLibrary.getUserById(8)));
        addAnReplyToPost(0, new Reply("How does something orbit around the earth if it is flat?", userLibrary.getUserById(8)));

        addAnReplyToPost(21, new Reply("How does something orbit around the earth if it is flat?", userLibrary.getUserById(8)));
        addAnReplyToPost(21, new Reply("How does something orbit around the earth if it is flat?", userLibrary.getUserById(8)));

        addAnReplyToPost(4, new Reply("If you read this nested replies work", userLibrary.getUserById(6)));
        addAnReplyToPost(2, new Reply("If you read this nested replies work", userLibrary.getUserById(6)));
        addAnReplyToPost(8, new Reply("If you read this nested replies work", userLibrary.getUserById(6)));
        addAnReplyToPost(6, new Reply("If you read this nested replies work", userLibrary.getUserById(6)));
    }

    static {
        initUsers();
        initArticles();
        initReplies();
    }
}
