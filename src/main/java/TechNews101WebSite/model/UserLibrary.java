package TechNews101WebSite.model;

import java.util.ArrayList;

public class UserLibrary {

    private ArrayList<User> users;

    public UserLibrary() {
        this.users = new ArrayList<>();
    }

    /**
     * check if a username already exists. if it doesn't adds the user to the user-library
     *
     * @param addingUser user that is being added
     */
    public void addUserWithUserNameCheck(User addingUser) {

        if (userNameCheck(addingUser.getUsername())) {
            System.out.println("** User has NOT been added, because Username Already Exists.");
        } else {
            addUser(addingUser);
            System.out.println("** User: " + addingUser + " has been added to User Database");
        }
    }

    /**
     * checks if a username exists
     *
     * @param username the checking username
     * @return
     */
    public boolean userNameCheck(String username) {
        for (User u : users) {
            if (u.getUsername().equals(username)) {
                return true;
            }
        }
        return false;
    }

    /**
     * simply adds a user
     *
     * @param user adding user
     */
    private void addUser(User user) {
        this.users.add(user);
    }

    public ArrayList<User> getAllUsers() {
        return users;
    }

    /**
     * returns a user based on its username, if one is not founds, returns null
     *
     * @param qUsername searching username
     * @return
     */
    public User getUserByUserName(String qUsername) {
        for (User user : users) {
            if (user.getUsername().equals(qUsername)) {
                return user;
            }
        }
        return null;
    }

    /**
     * returns a user based on its id, if one is not founds, returns null
     *
     * @param id searching id
     * @return
     */
    public User getUserById(int id) {
        User returningUser = null;

        for (User user : users) {
            if (user.getId() == id) {
                returningUser = user;
            }
        }

        return returningUser;
    }
}
