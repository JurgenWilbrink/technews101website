package TechNews101WebSite.model;

import TechNews101WebSite.staticstuff.DataClass;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class ArticleLibraby {

    //Article extends from reply therefor this list of articles is actually a list of 'replies'
    private static ArrayList<Article> articles;

    public ArticleLibraby() {
        this.articles = new ArrayList<>();
    }

    /**
     * this method adds a reply to a post
     *
     * @param targetId the target post you want the reply to be attached to.
     * @param reply    the actual reply text
     * @return
     */
    public Reply addReply(int targetId, Reply reply) {
        Reply targetReply = findReplyById(targetId, new ArrayList<Reply>(articles));
        if (targetReply != null) {
            targetReply.addReply(reply);
            return reply;
        } else {
            System.out.println("** null... " + targetReply);
        }
        return null;
    }

    /**
     * this method finds the root article of an reply.
     *
     * @param targetId this is the reply you want to find the root article for.
     * @return
     */
    public Article findRootArticleByReplyId(int targetId) {
        for (Article article : articles) {
            if (findReplyById(targetId, new ArrayList<Reply>(Arrays.asList(article))) != null) {
                System.out.println(">> Searched Article Root: " + article);
                return article;
            }
        }
        return null;
    }

    /**
     * this is a recursive method to find any reply to a post.
     *
     * @param targetId the reply you want to find.
     * @param replies  the parent replies
     * @return
     */
    public Reply findReplyById(int targetId, ArrayList<Reply> replies) {
        for (Reply reply : replies) {
            if (reply.getPostId() == targetId) {
                return reply;
            } else {
                Reply childReply = findReplyById(targetId, reply.getReplies());
                if (childReply != null) return childReply;
            }
        }
        return null;
    }

    /**
     * this method returns all articles that have a match to the searchword.
     *
     * @param searchword the word(group) you are searching for
     * @return
     */
    public ArrayList<Article> getArticlesBySearchword(String searchword) {

        //creating values to help in separating the articles
        ArrayList<Article> returningArrayList = new ArrayList<>();

        //run this for all articles.
        for (int i = 0; i < DataClass.articleLibraby.getArticles().size(); i++) {

            //getting a single article discription
            String thisArticle = DataClass.articleLibraby.getArticles().get(i).getDescription();

            //if that discription contains the searchword save the artcile in the return arraylist
            if (thisArticle.contains(searchword)) {
                returningArrayList.add(DataClass.articleLibraby.getArticles().get(i));
            }
        }
        return returningArrayList;
    }

    /**
     * this method returns articles in A to Z or Z to A order.
     *
     * @param AtoZ     based on this boolean the articles are sorted A to Z or Z to A
     * @param articles these articles will get sorted
     * @return
     */
    public ArrayList<Article> getArticlesFromAtoZ(boolean AtoZ, ArrayList<Article> articles) {
        articles.sort(Comparator.comparing(Article::getTitle));
        if (!AtoZ) {
            Collections.reverse(articles);
        }
        return articles;
    }

    /**
     * this method simply adds an article to the article-library
     *
     * @param article the article being added
     */
    public void addArticleToLibrary(Article article) {
        articles.add(article);
        System.out.println("** Article: " + article + " Added");
    }

    /**
     * this method returns all articles with a special category
     *
     * @param category the category searching for.
     * @return
     */
    public ArrayList<Article> getArticlesWithCategory(Article.Category category) {
        System.out.println(category);
        // creating the returning ArrayList
        ArrayList<Article> returningArticles = new ArrayList<>();

        // loop through all categories of all articles
        for (Article article : DataClass.articleLibraby.getArticles()) {
            for (int i = 0; i < article.getCategories().size(); i++) {
                if (article.getCategories().get(i).equals(category)) {

                    //add them to the returning ArrayList if the searching article matches one of the categories the article has
                    returningArticles.add(article);
                }
            }
        }
        return returningArticles;
    }

    public ArrayList<Article> getArticles() {
        return articles;
    }

    /**
     * searches for an article with an id.
     *
     * @param id id you are searching for
     * @return
     */
    public Article getArticleWithID(int id) {

        //loop through all articles
        for (Article article : articles) {
            if (article.getPostId() == id) {

                //if the ID matches set the returning article
                System.out.println("** found Article: " + article);
                return article;
            }
        }

        System.out.println("** did not find an article with id: " + id);
        return null;
    }

    public ArrayList<Article> getMostRecentArticles(int amount) {

        //getting the list with articles
        ArrayList<Article> tempList = DataClass.articleLibraby.getArticles();

        //reverse the list with articles
        Collections.reverse(tempList);

        //get the given amount of most recent articles
        ArrayList<Article> anotherTempList = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            anotherTempList.add(tempList.get(i));
        }

        //return the most recent articles
        return anotherTempList;
    }

    /**
     * deletes an article when a correct Id is given.
     *
     * @param targetDeletionId the article to be deleted.
     * @return returns weather it succeeded to delete the article
     */
    public boolean deleteArticle(int targetDeletionId) {

        if (getArticleWithID(targetDeletionId) == null) {
            return false;
        } else {
            for (int i = 0; i < articles.size(); i++) {
                Article article = articles.get(i);

                if (article.getPostId() == targetDeletionId) {
                    articles.remove(i);
                }
            }
            System.out.println("XXX article #" + targetDeletionId + " has been deleted!");
            return true;
        }
    }

    public ArrayList<Article> getThreeArticles() {
        ArrayList<Article> newestArticles = new ArrayList<>();
        newestArticles.add(articles.get(articles.size() - 1));
        newestArticles.add(articles.get(articles.size() - 2));
        newestArticles.add(articles.get(articles.size() - 3));
        return newestArticles;
    }
}
