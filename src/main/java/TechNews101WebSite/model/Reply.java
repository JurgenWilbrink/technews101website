package TechNews101WebSite.model;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Reply {

    private int postId;
    private String postText;
    private User postAuthor;
    private LocalDateTime publishTimeDate;
    private ArrayList<Reply> replies;

    private static int lastAssignedId = 0;

    /**
     * constructor of a reply.
     *
     * @param postText   the text of the reply
     * @param postAuthor the author of the reply
     */
    public Reply(String postText, User postAuthor) {
        this.postText = postText;
        this.postAuthor = postAuthor;
        this.publishTimeDate = LocalDateTime.now();
        this.replies = new ArrayList<>();
        postId = lastAssignedId++;
    }

    /**
     * default constructor
     */
    public Reply() {
        postId = lastAssignedId++;
        this.publishTimeDate = LocalDateTime.now();
        replies = new ArrayList<>();
    }

    public int getPostId() {
        return postId;
    }

    /**
     * adds a reply to the replies
     *
     * @param reply adding reply
     */
    public void addReply(Reply reply) {
        replies.add(reply);
        System.out.println("** Reply added to post: " + this.getPostId());
    }

    public String getPostText() {
        return postText;
    }

    public void setPostText(String postText) {
        this.postText = postText;
    }

    public User getPostAuthor() {
        return postAuthor;
    }

    public void setPostAuthor(User postAuthor) {
        this.postAuthor = postAuthor;
    }

    public LocalDateTime getPublishTimeDate() {
        return publishTimeDate;
    }

    public void setPublishTimeDate(LocalDateTime publishTimeDate) {
        this.publishTimeDate = publishTimeDate;
    }

    public ArrayList<Reply> getReplies() {
        return replies;
    }

    public void setReplies(ArrayList<Reply> replies) {
        this.replies = replies;
    }

    public static int getLastAssignedPostId() {
        return lastAssignedId;
    }

    @Override
    public String toString() {
        return "Reply{" +
                "postId=" + postId +
                ", postText='" + postText + '\'' +
                ", postAuthor=" + postAuthor +
                ", publishTimeDate=" + publishTimeDate +
                ", replies=" + replies +
                '}';
    }

}
