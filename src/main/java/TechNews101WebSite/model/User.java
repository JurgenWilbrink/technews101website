package TechNews101WebSite.model;

import java.time.LocalDate;
import java.util.ArrayList;

public class User {

    private String username;
    private String realname;
    private String password;
    private int age;
    private int activityCount;
    private LocalDate joinDate;
    ArrayList<Article> favorites;
    private int id;

    static int lastAssignedId = 0;

    /**
     * this constructor is used to generate Users before-hand. therefore the activityCount needs to be inserted here.
     *
     * @param username
     * @param realname
     * @param password
     * @param age
     * @param activityCount
     */
    public User(String username, String realname, String password, int age, int activityCount) {
        this.username = username;
        this.realname = realname;
        this.password = password;
        this.age = age;
        this.activityCount = activityCount;
        this.joinDate = LocalDate.now();
        this.favorites = new ArrayList<>();
        this.id = lastAssignedId++;
    }

    /**
     * this constructor is used to register users while the site is running, when the users 'just join', therefor their activityCount starts at 0.
     *
     * @param username
     * @param realname
     * @param password
     * @param age
     */
    public User(String username, String realname, String password, int age) {
        this.username = username;
        this.realname = realname;
        this.password = password;
        this.age = age;
        this.activityCount = 0;
        this.joinDate = LocalDate.now();
        this.favorites = new ArrayList<>();
        this.id = lastAssignedId++;
    }

    /**
     * default constructor
     */
    public User() {
        this.id = lastAssignedId++;
        this.joinDate = LocalDate.now();
        this.favorites = new ArrayList<>();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getActivityCount() {
        return activityCount;
    }

    public void setActivityCount(int addingfactor) {
        this.activityCount = addingfactor;
    }

    public LocalDate getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(LocalDate joinDate) {
        this.joinDate = joinDate;
    }

    public ArrayList<Article> getFavorites() {
        return favorites;
    }

    public void setFavorites(ArrayList<Article> favorites) {
        this.favorites = favorites;
    }

    public int getId() {
        return id;
    }
}
