package TechNews101WebSite.model;

public class Administrator extends User {

    private int adminId;
    private String adminPass;
    private boolean allowDeletion;

    /**
     * An administrator is basicly a normal user but then with some more information
     * the extra things are adminId, adminPass and allowDeletion.
     * all the other Parameters are user to create the base user in the super class
     *
     * @param username
     * @param realname
     * @param password
     * @param age
     * @param activityCount the amount of times this user has visited an article page.
     * @param adminId       special administrator id
     * @param adminPass     special administrator password
     * @param allowDeletion
     */

    public Administrator(String username, String realname, String password, int age, int activityCount, int adminId, String adminPass, boolean allowDeletion) {
        super(username, realname, password, age, activityCount);
        this.adminId = adminId;
        this.adminPass = adminPass;
        this.allowDeletion = allowDeletion;
    }

    public int getAdminId() {
        return adminId;
    }

    public void setAdminId(int adminId) {
        this.adminId = adminId;
    }

    public String getAdminPass() {
        return adminPass;
    }

    public void setAdminPass(String adminPass) {
        this.adminPass = adminPass;
    }

    public boolean isAllowDeletion() {
        return allowDeletion;
    }

    public void setAllowDeletion(boolean allowDeletion) {
        this.allowDeletion = allowDeletion;
    }
}
