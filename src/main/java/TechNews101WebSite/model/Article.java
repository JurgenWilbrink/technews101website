package TechNews101WebSite.model;

import java.util.ArrayList;


public class Article extends Reply {

    /**
     * this is the enum Category of Article.
     * an article always has to have a category associated with it.
     */
    public enum Category {
        GAMES("Games"),
        SPACE("Space"),
        SOCIALMEDIA("SocialMedia"),
        GENERAL("General");

        private String stringVal;

        Category(String val) {
            this.stringVal = val;
        }

        public String getStringVal() {
            return stringVal;
        }
    }

    private String title;
    private String description;
    private String picturePath;
    private ArrayList<Category> categories;
    private String imageAlt;

    /**
     * an article is basicly a glorified reply.
     *
     * @param title       article title
     * @param description article description
     * @param postText    Actual article text
     * @param postAuthor  author of the article
     * @param picturePath the location of an image
     * @param imageAlt    the description of an image
     * @param categories  the categories associated with an article
     */
    public Article(String title, String description, String postText, User postAuthor, String picturePath, String imageAlt, ArrayList<Category> categories) {
        super(postText, postAuthor);
        this.title = title;
        this.description = description;
        if (picturePath == null) {
            this.picturePath = "/images/article.jpg";
            this.imageAlt = "standard article image";
        } else {
            this.picturePath = picturePath;
            this.imageAlt = imageAlt;
        }
        this.categories = categories;
    }

    //default constructor
    //not sure about this tho
    public Article() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    public ArrayList<Category> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String discription) {
        this.description = discription;
    }

    public String getImageAlt() {
        return imageAlt;
    }

    public void setImageAlt(String imageAlt) {
        this.imageAlt = imageAlt;
    }
}
