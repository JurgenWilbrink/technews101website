package TechNews101WebSite.controllers;

import TechNews101WebSite.model.Article;
import TechNews101WebSite.staticstuff.DataClass;
import TechNews101WebSite.staticstuff.Util;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;

/**
 * this is the controller for everything that has to do with everything general
 */
@Controller
@RequestMapping("/technews")
public class MainWebsiteController {
    boolean AtoZ = true;

    /**
     * this method returns you to the home screen
     *
     * @param session
     * @param model
     * @return
     */
    @GetMapping(path = "/home")
    public String getHomePage(HttpSession session, Model model) {
        Util.checkForSessionAddAtributesAccordingly(session, model);
        ArrayList<Article> articles = new ArrayList<>();
        articles = DataClass.articleLibraby.getThreeArticles();

        model.addAttribute("articles", articles);

        return "main";
    }

    /**
     * this method returns an article with a specific category
     * if you just press category in the topbar you go to the general category
     *
     * @param category the category of the articles
     * @param session
     * @param model
     * @return
     */
    //otherwise this path will be taken, if no Existing category is entered, it will return General Articles
    //This can be found in Util.getEnumCategoryFromString
    @GetMapping(path = "/category/{category}")
    public String getCategoryPage(@PathVariable String category, HttpSession session, Model model) {
        Util.checkForSessionAddAtributesAccordingly(session, model);
        int id = 1;
        Article.Category category1 = Util.getEnumCategoryFromString(category);
        ArrayList<Article> sortedList = DataClass.articleLibraby.getArticlesWithCategory(category1);
        if (category.equals("general")) {
            id = 1;
        } else if (category.equals("games")) {
            id = 2;
        } else if (category.equals("space")) {
            id = 3;
        } else if (category.equals("socialmedia")) {
            id = 4;
        }
        model.addAttribute("whichCategory", id);
        model.addAttribute("articles", sortedList);

        return "category";
    }

    /**
     * this method looks for articles with your searchword
     *
     * @param searchword the word youre looking for
     * @param model
     * @param session
     * @return
     */
    @GetMapping(path = "search")
    public String getArticleBySearchword(@RequestParam("q") String searchword, Model model, HttpSession session) {
        Util.checkForSessionAddAtributesAccordingly(session, model);

        ArrayList<Article> results = DataClass.articleLibraby.getArticlesBySearchword(searchword);
        int amountOfResults = results.size();
        model.addAttribute("searchword", searchword);
        model.addAttribute("amount", amountOfResults);
        if (AtoZ) { //van a tot z georderd
            model.addAttribute("boolean", true);
            results = DataClass.articleLibraby.getArticlesFromAtoZ(AtoZ, results);
            model.addAttribute("articles", results);
            AtoZ = false;
        } else if (!AtoZ) { // van z tot a georderd
            model.addAttribute("boolean", false);
            results = DataClass.articleLibraby.getArticlesFromAtoZ(AtoZ, results);
            model.addAttribute("articles", results);
            AtoZ = true;
        }
        return "search";
    }

    //TODO login (later)
}