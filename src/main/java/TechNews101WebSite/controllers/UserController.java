package TechNews101WebSite.controllers;

import TechNews101WebSite.staticstuff.DataClass;
import TechNews101WebSite.model.User;
import TechNews101WebSite.staticstuff.Util;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * this is the controller for everything that has to do with users
 */
@Controller
@RequestMapping(path = "/user")
public class UserController {

    /**
     * this method returns a from for you to insert your information
     *
     * @param session
     * @param model
     * @return
     */
    @GetMapping(path = "/register")
    public String registerUser(HttpSession session, Model model) {
        Util.checkForSessionAddAtributesAccordingly(session, model);
        return "register";
    }

    /**
     * this method checks the form
     * if it is good you get signed up
     * if not you get a new form
     *
     * @param request
     * @param model
     * @param session
     * @return
     */
    //the parameters inserted in the form of the page returned above are used here to create a new user.
    @PostMapping(path = "/register")
    public String registerUser(HttpServletRequest request, Model model, HttpSession session) {
        Util.checkForSessionAddAtributesAccordingly(session, model);

        //get parameters
        String confirmpassword = request.getParameter("confirmpassword");
        String password = request.getParameter("password");
        String username = request.getParameter("username");
        String realname = request.getParameter("realname");
        int age = Integer.parseInt(request.getParameter("age"));

        //check for name lengths
        if (username.length() < 4 || realname.length() < 4) {
            model.addAttribute("responseString", "Username and Real name should both have more then 4 characters!");
            System.out.println("That Associated Model: " + model);
            return "register";
        }

        //check password length
        if (password.length() < 4) {
            model.addAttribute("responseString", "The password needs to contain at least 4 characters!");
            model.addAttribute("previousUsername", username);
            model.addAttribute("previousRealname", realname);
            model.addAttribute("previousAge", age);
            return "register";
        }

        //check-password
        if (!confirmpassword.equals(password)) {
            model.addAttribute("responseString", "The passwords do not match!");
            model.addAttribute("previousUsername", username);
            model.addAttribute("previousRealname", realname);
            model.addAttribute("previousAge", age);
            System.out.println("That Associated Model: " + model);
            return "register";
        }

        User user = new User(username, realname, password, age);

        System.out.println(user + ", " + confirmpassword);

        DataClass.userLibrary.addUserWithUserNameCheck(user);

        return "redirect:/user/" + user.getId();
    }

    /**
     * laat een enkele gebruiker zien.
     *
     * @param model
     * @param id    user id
     * @return
     */
    @GetMapping(path = "/{id}")
    public String getUserPage(Model model, @PathVariable int id, HttpSession session) {
        Util.checkForSessionAddAtributesAccordingly(session, model);

        try {
            User thisUser = DataClass.userLibrary.getUserById(id);
            model.addAttribute("user", thisUser);
            System.out.println("> Showing Page of User: " + thisUser);
            return "user";
        } catch (Exception e) {
            return "redirect:/technews/home";
        }
    }

    /**
     * this method returns a form if you want to sign in
     *
     * @param session
     * @param model
     * @return
     */
    @GetMapping(path = "/login")
    public String getLoginForm(HttpSession session, Model model) {
        Util.checkForSessionAddAtributesAccordingly(session, model);
        return "login";
    }


    /**
     * this method checks the form
     * if its good you get signed in
     * if not you get a new form
     *
     * @param request
     * @param session
     * @param model
     * @return
     */
    @PostMapping(path = "/login")
    public String loginUser(HttpServletRequest request, HttpSession session, Model model) {
        Util.checkForSessionAddAtributesAccordingly(session, model);

        String inputUsername = request.getParameter("username");
        String inputPassword = request.getParameter("password");

        System.out.println("-> LoginStuff username input: " + inputUsername + ", passwordinput: " + inputPassword);

        User inloggingUser = DataClass.userLibrary.getUserByUserName(inputUsername);

        System.out.println("-> LoginStuff User inloggingUser: " + inloggingUser);

        if (inloggingUser == null) {
            model.addAttribute("responseString", "There is no user found with that Username!");
            return "login";
        }

        String requeredPassword = inloggingUser.getPassword();

        System.out.println("-> LoginStuff Required password: " + requeredPassword);

        //test the password
        if (!inputPassword.equals(requeredPassword)) {
            model.addAttribute("responseString", "The password is not correct!");
            return "login";
        }

        session.setAttribute("username", inloggingUser.getUsername());

        System.out.println("@@Session created with value: " + inloggingUser.getUsername());

        return "redirect:/user/profile";
    }

    /**
     * this method shows your user profile if youre logged in
     * if youre not logged in you will be redirected to the sign in screen
     *
     * @param session
     * @param model
     * @return
     */
    @GetMapping(path = "/profile")
    public String showUserProfile(HttpSession session, Model model) {

        Util.checkForSessionAddAtributesAccordingly(session, model);

        if (Util.sessionCheck(session)) {
            String username = String.valueOf(session.getAttribute("username"));
            User user = DataClass.userLibrary.getUserByUserName(username);
            model.addAttribute("user", user);

            return "profile";
        } else {
            return "register";
        }
    }

    /**
     * this method lets you logout of your profile
     *
     * @param session
     * @param model
     * @return
     */
    @GetMapping("/logout")
    public String logout(HttpSession session, Model model) {
        Util.checkForSessionAddAtributesAccordingly(session, model);

        session.invalidate();
        return "redirect:/user/login";
    }

}
