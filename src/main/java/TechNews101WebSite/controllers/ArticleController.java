package TechNews101WebSite.controllers;

import TechNews101WebSite.model.*;
import TechNews101WebSite.staticstuff.DataClass;
import TechNews101WebSite.staticstuff.Util;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;

/**
 * this is the controller for everything that has to do with articles.
 */
@Controller
@RequestMapping(path = "/article")
public class ArticleController {

    /**
     * this methods returns a form for you to add an article
     * and if youre not yet logged in to an account it firsts lets you log on
     *
     * @param session
     * @param model
     * @return
     */
    @GetMapping(path = "/add")
    public String addArticle(HttpSession session, Model model) {
        Util.checkForSessionAddAtributesAccordingly(session, model);
        if (Util.sessionCheck(session)) {
            return "addarticle";
        } else {
            return "redirect:/user/register";
        }
    }

    /**
     * this method checks the form and if its good it adds the article
     * if its not good it will return a new form to the user
     *
     * @param request
     * @param session
     * @param model
     * @return
     */
    @PostMapping(path = "/add")
    public String addArticle(HttpServletRequest request, HttpSession session, Model model) {
        Util.checkForSessionAddAtributesAccordingly(session, model);
        System.out.println("test");
        if (Util.sessionCheck(session)) {

            String usernameString = String.valueOf(session.getAttribute("username"));

            User author = DataClass.userLibrary.getUserByUserName(usernameString);

            String title = request.getParameter("title");
            String description = request.getParameter("description");
            String text = request.getParameter("text");
            String picturePath = request.getParameter("picturepath");
            String imagealt = request.getParameter("imagealt");

            if (picturePath.equals("")) {
                picturePath = null;
            }
            if (imagealt.equals("")) {
                imagealt = null;
            }

            System.out.println("PicturestuffTest: path: '" + picturePath + "', imageAlt: '" + imagealt + "'");

            // picture path = something, and image alt is nothing then return an 'error'
            if (picturePath != null && imagealt == null) {
                model.addAttribute("responseString", "if custom image is used enter a description of it!");
                return "addarticle";
            }

            // picture path = nothing, and image alt is something then return an 'error'
            if ((imagealt != null && picturePath == null)) {
                model.addAttribute("responseString", "U can not have an image description without an image!");
                return "addarticle";
            }

            boolean checkboxGeneral = Boolean.parseBoolean(request.getParameter("checkgeneral"));
            boolean checkboxGames = Boolean.parseBoolean(request.getParameter("checkgames"));
            boolean checkboxSpace = Boolean.parseBoolean(request.getParameter("checkspace"));
            boolean checkboxSocialMedia = Boolean.parseBoolean(request.getParameter("checksocialmedia"));

            ArrayList<Article.Category> categories = new ArrayList<>();

            if (checkboxGeneral) {
                categories.add(Article.Category.GENERAL);
            }
            if (checkboxGames) {
                categories.add(Article.Category.GAMES);
            }
            if (checkboxSpace) {
                categories.add(Article.Category.SPACE);
            }
            if (checkboxSocialMedia) {
                categories.add(Article.Category.SOCIALMEDIA);
            }

            if (!checkboxGeneral && !checkboxGames && !checkboxSpace && !checkboxSocialMedia) {
                categories.add(Article.Category.GENERAL);
            }

            System.out.println("total: " + categories);

            Article article = new Article(title, description, text, author, picturePath, imagealt, categories);

            DataClass.articleLibraby.addArticleToLibrary(article);
            return "redirect:/article/" + article.getPostId() + "/";
        } else {
            return "redirect:/user/register";
        }
    }

    /**
     * this method returns a single article based on an unique id
     *
     * @param response
     * @param counter  how often the user visited that article
     * @param model
     * @param id       the unique article id
     * @param session
     * @return
     */
    @GetMapping(path = "/{id}")
    public String getArticleById(HttpServletResponse response, @CookieValue(value = "counter", defaultValue = "0") int counter,
                                 Model model, @PathVariable Integer id, HttpSession session) {
        Util.checkForSessionAddAtributesAccordingly(session, model);

        //adds 1 for every time an article has been visited
        if (Util.sessionCheck(session)) {
            counter++;

            DataClass.userLibrary.getUserByUserName(String.valueOf(session.getAttribute("username"))).setActivityCount(counter);

            Cookie cookie = new Cookie("counter", "" + counter);

            System.out.println("## cookieValue:" + cookie.getValue());

            //that is 2 weeks
            cookie.setMaxAge(1209600);
            response.addCookie(cookie);
        }

        try {
            Article thisArticle = DataClass.articleLibraby.getArticleWithID(id);
            model.addAttribute("article", thisArticle);
            System.out.println("> showing a single article: " + thisArticle);

            if (Util.checkSessionForAdministrator(session)) {
                model.addAttribute("allowDeletion", true);
            }

            return "article";
        } catch (Exception e) {
            System.out.println("!!Caught exception: " + e);
            return "redirect:/technews/home";
        }
    }

    /**
     * this method shows a form to add a reply if you are not logged on yet it redirects you to te log on screen
     *
     * @param model
     * @param session
     * @return
     */
    @GetMapping(path = "/reply")
    public String addReplyToAnotherPost(Model model, HttpSession session) {
        Util.checkForSessionAddAtributesAccordingly(session, model);
        if (Util.sessionCheck(session)) {
            model.addAttribute("articleCount", Reply.getLastAssignedPostId());
            return "addreply";
        } else {
            return "redirect:/user/register";
        }
    }

    /**
     * this method checks the form if its good
     * if its good it adds the reply
     * if not it returns a new form
     *
     * @param request
     * @param session
     * @param model
     * @return
     */
    @PostMapping(path = "/reply")
    public String addReplyToAnotherPost(HttpServletRequest request, HttpSession session, Model model) {
        Util.checkForSessionAddAtributesAccordingly(session, model);
        if (Util.sessionCheck(session)) {

            String text = request.getParameter("text");

            String usernameString = String.valueOf(session.getAttribute("username"));

            int replyId = Integer.parseInt(request.getParameter("id"));

            if (DataClass.articleLibraby.findReplyById(replyId, new ArrayList<>(DataClass.articleLibraby.getArticles())) == null) {
                model.addAttribute("responseString", "No Post has been found for you Target ID!");
                return "addreply";
            }

            User author = DataClass.userLibrary.getUserByUserName(usernameString);

            DataClass.articleLibraby.addReply(replyId, new Reply(text, author));

            int rootArticleId = DataClass.articleLibraby.findRootArticleByReplyId(replyId).getPostId();

            return "redirect:/article/" + rootArticleId + "/";
        } else {
            return "redirect:/user/register";
        }
    }

    /**
     * this method returns all the articles in our dataclass
     *
     * @param model
     * @param session
     * @return
     */
    @GetMapping(path = "/all")
    public String getAllArticle(Model model, HttpSession session) {
        Util.checkForSessionAddAtributesAccordingly(session, model);
        ArrayList<Article> allArticles = DataClass.articleLibraby.getArticles();
        model.addAttribute("articles", allArticles);
        System.out.println("> showing all articles: " + allArticles);

        for (int i = 0; i < allArticles.size(); i++) {
            System.out.println("*TitleTest: " + i + ", " + allArticles.get(i).getTitle());
        }

        return "allarticles";
    }

    /**
     * this method lets you delete an article by using the id to find the right article
     * you can only do this if your an administrator
     *
     * @param id      the unique id of the article
     * @param session
     * @param model
     * @return
     */
    @PostMapping(path = "/{id}/delete")
    public String deleteArticle(@PathVariable int id, HttpSession session, Model model) {
        Util.checkForSessionAddAtributesAccordingly(session, model);

        if (Util.checkSessionForAdministrator(session)) {
            DataClass.articleLibraby.deleteArticle(id);
            model.addAttribute("deletionString", "Article #" + id + " has been deleted! all articles has moved 1 ID down.");
            return "redirect:/article/all";
        }

        return "redirect:/article/" + id;
    }
}
